#! /bin/bash

#script um isochronen zu erzeugen
#


#Delta des abstande der Abfrage in Minuten
delta=1
weite=900 #15 minuten
#datum
date="2011-10-12"
hour="12"
minutes="30"

num=12
mkdir abfragen
mkdir abfragen/$date
mkdir abfragen/$date/$weite

i=0

while [[ $i -lt $num ]]
do

# Ischrone request that requires only one parameter: server
#server="http://localhost:8080/testing/rest/isochrone"
server="http://dbis-isochrone.uibk.ac.at:8080/testing/rest/isochrone"
hourstring=$hour
if [[ $hour -lt 10 ]]
then
    hourstring="0"$hour
fi

minutestring=$minutes
if [[ $minutes -lt 10 ]]
then
    minutestring="0"$minutes
fi


datetime=$date"T"$hourstring":"$minutestring

clientid=`date +%s%N`

echo "Requesting $datetime"
sleep 1

curl -s -o abfragen/$date/$weite/$datetime§$weite.json \
  -H "ClientId: $clientid"\
  -H "Accept: application/json"\
  -H "Entity: {\"algorithm\":\"MINEX\",\
               \"coverageMode\":\"SURFACE\",\
               \"dataset\":\"bz\",\
               \"dateTime\":\"$datetime\",\
               \"direction\":\"INCOMING\",\
               \"dmax\":$weite,\
               \"expirationMode\":false,\
               \"maxMemorySize\":0,\
               \"mode\":\"MULTIMODAL\",\
               \"queryNodes\":[[11.3385784,46.5027513]],\
               \"walkingSpeed\":4.5}"\
   $server && echo "done $datetime" &






minutes=$((minutes + $delta))

if [[ $(($minutes / 60)) -gt 0 ]]
then
    hour=$(($hour + 1))
    minutes=$(($minutes - 60))
fi

i=$((i+1))

done


wait








exit


