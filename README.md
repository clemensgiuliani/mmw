Source code of our mmw 2015 Isochrones Project.

json files can be imported in Qgis, 
set style to graduated with column frequency and adjust number of classes.

branch master: calculates mittlere Isochrone
(now merged; see project skew in master )branch skew : implementation of skewness calculation on isochrone intersections

sample files can be found in scripts/ for each branch

developed with MonoDevelop (http://www.monodevelop.com/)
requires mono to run (http://www.mono-project.com) or .net Framework on Windows (not tested)