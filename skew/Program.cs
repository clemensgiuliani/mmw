﻿using System;
using System.IO;
using Newtonsoft.Json;
using ClipperLib;
using GeoJSON;
using GeoJSON.Net.Geometry;
using Newtonsoft.Json.Serialization;
using System.Linq;
using System.Collections.Generic;
using GeoJSON.Net.Feature;
using IsochroneLib;

namespace ClipperMMW
{
	using Path = List<IntPoint>;
	using Paths = List<List<IntPoint>>;
	using ListOfPolygons = List<List<List<IntPoint>>>;

	class Program
	{	
		static Isochrone Intersection (Isochrone a, Isochrone b){
			Clipper c = new Clipper ();
			Isochrone result = new Isochrone ();
			c.AddPaths (a.isochronePaths,PolyType.ptSubject,true);
			c.AddPaths (b.isochronePaths,PolyType.ptClip,true);
			c.Execute (ClipType.ctIntersection, result.isochronePaths);
			//result.times = 
			DateTime [] tempdt = a.times.ToArray();
			Array.Resize (ref tempdt, a.times.Count + 1);
			tempdt [a.times.Count] = b.queryTime;
			//result.times.Add(b.queryTime);
			result.times = tempdt.ToList ();
			return result;
		}
		static Isochrone Union(Isochrone a, Isochrone b){
			Clipper c = new Clipper ();
			Isochrone result = new Isochrone ();
			c.AddPaths (a.isochronePaths,PolyType.ptSubject,true);
			c.AddPaths (b.isochronePaths,PolyType.ptClip,true);
			c.Execute (ClipType.ctUnion, result.isochronePaths);
			//result.times = a.times;
			result.times = a.times;
			//result.times.add (b.queryTime);
			return result;
		}
		static Isochrone Difference (Isochrone a, Isochrone b ){
			Clipper c = new Clipper ();
			Isochrone result = new Isochrone ();
			c.AddPaths (a.isochronePaths,PolyType.ptSubject,true);
			c.AddPaths (b.isochronePaths,PolyType.ptClip,true);
			c.Execute (ClipType.ctDifference, result.isochronePaths);
			result.times = a.times;
			//result.times.add (b.queryTime);
			return result;
		}
		static Isochrone XOR(Isochrone a, Isochrone b){
			Clipper c = new Clipper ();
			Isochrone result = new Isochrone ();
			c.AddPaths (a.isochronePaths, PolyType.ptSubject, true);
			c.AddPaths (b.isochronePaths, PolyType.ptClip, true);
			c.Execute (ClipType.ctXor, result.isochronePaths);
			//result.times = a.times;
			//result.times.add (b.queryTime);
			return result;
		}
		static void Main(string [] args){
			Console.WriteLine("usage: mono skew.exe inputfolder/ out.json true");
			Console.WriteLine ("filenames need to be like this: 2011-10-12T07:30§1200.json");
			bool doCleanup = Convert.ToBoolean(args [2]);
			DateTime start = DateTime.Now;
			Isochrones alliso = new Isochrones (args[0], true);
			DateTime endLoad = DateTime.Now;
			List<DateTime> alltimes = new List<DateTime>();
			foreach (Isochrone iso in alliso.list) {
				alltimes.Add (iso.queryTime);
			}
			alltimes.OrderBy(at=>at.Ticks);

			DateTime first = alltimes.First();
			DateTime last = alltimes.Last();
			int isochroneIDCounter = 0;
			Isochrones result = new Isochrones ();
			result.list.Add (alliso.list.First ());
			int max1 = alliso.list.Count ();
			DateTime startTime = DateTime.Now;
			for (int i = 1; i < max1; i++) {
				int max2 = result.list.Count ();
				for (int j = max2-1; j >=0; j--) {

					Isochrone temp = Intersection (result.list.ElementAt (j),alliso.list.ElementAt (i));

					if (temp.isochronePaths.Count > 0){
						temp.id = isochroneIDCounter;
						isochroneIDCounter++;
						result.list.Add (temp);
					}
					temp = Difference (result.list.ElementAt (j), alliso.list.ElementAt (i));

					if (temp.isochronePaths.Count > 0){
						result.list.Add (temp);
						temp.id = isochroneIDCounter;
						isochroneIDCounter++;
					}

					temp = alliso.list.ElementAt (i);
					foreach(Isochrone iso in result.list){
						Isochrone temp2 = temp;
						temp = Difference (temp2,iso);
					}

					if (temp.isochronePaths.Count > 0){
						temp.id = isochroneIDCounter;
						isochroneIDCounter++;
						result.list.Add (temp);
					}
					result.list.RemoveAt (j);
					if (doCleanup)
						result.cleanup ();

				}
				Console.WriteLine ((i+1)+","+(DateTime.Now-startTime).TotalMilliseconds);
			}
			DateTime endTime = DateTime.Now;
			Console.WriteLine ("cleanup");
			result.ToFile (args[1],first,last);
			DateTime saveTime = DateTime.Now;
			Console.WriteLine ("executed. Output: " + args [1]);
			Console.WriteLine("loadtime= "+endLoad.Subtract(start));
			Console.WriteLine("calc= "+endTime.Subtract(endLoad));
			Console.WriteLine("save= "+saveTime.Subtract(endTime));
			/*
			for (int i = 0; i < result.Count(); i++) {
				result.list.ElementAt (i).ToFile ("/home/mmw/verteilung/file"+i+".json");
			}*/
		}
	}
}