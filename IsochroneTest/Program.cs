﻿using System;
using System.IO;
using Newtonsoft.Json;
using ClipperLib;
using GeoJSON;
using GeoJSON.Net.Geometry;
using Newtonsoft.Json.Serialization;
using System.Linq;
using System.Collections.Generic;
using GeoJSON.Net.Feature;
using IsochroneLib;

namespace ClipperMMW
{
	using Path = List<IntPoint>;
	using Paths = List<List<IntPoint>>;
	using ListOfPolygons = List<List<List<IntPoint>>>;

	class Program
	{	
		//TODO anzahl punkte reduzieren/clean
		//TODO add testdata

		static void Main(string [] args){

			Isochrones alliso = new Isochrones ("/home/mmw/output/900/",false);
			Isochrones[] iso = new Isochrones[4];
			for (int i = 0; i < 4; i++) {
				iso [i] = new Isochrones ();
				iso [i].list = new List<Isochrone> ();
			}
			int total = alliso.Count();
			for (int i = 0; i < total; i++) {
				int k = Convert.ToInt32 (Math.Round ((double)4 * i / total-0.49));
				//Console.WriteLine (k);
				iso [k].list.Add (alliso.list.ElementAt(i));
				//alliso
			}

			Clipper c = new Clipper ();
			for (int i = 0; i < 4; i++) {
				c.AddPaths (iso [i].getIsochroneWithProbability ((double)0.5).isochronePaths, PolyType.ptSubject, true);
				Console.WriteLine (i);
				iso [i].getIsochroneWithProbability ((double)0.5).ToFile ("/tmp/" + i + ".json");
			}
			Isochrones all50 = new Isochrones ();
			for (int i = 0; i < 4;i++) {
				all50.list.Add(iso [i].getIsochroneWithProbability ((double)0.5));
		
			}
			all50.intersect ();
			all50.sortByFreq ();
			all50.ToFile("/tmp/allmedia.json");
			all50.getIsochroneWithProbability (1.0).ToFile ("/tmp/media.json");

		}
	} 
}

