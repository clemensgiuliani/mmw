﻿using System;
using System.IO;
using Newtonsoft.Json;
using ClipperLib;
using GeoJSON;
using GeoJSON.Net.Geometry;
using Newtonsoft.Json.Serialization;
using System.Linq;
using System.Collections.Generic;
using GeoJSON.Net.Feature;
using IsochroneLib;

namespace ClipperMMW
{
	using Path = List<IntPoint>;
	using Paths = List<List<IntPoint>>;
	using ListOfPolygons = List<List<List<IntPoint>>>;

	class Program
	{	
		//TODO anzahl punkte reduzieren/clean
		//TODO add testdata

		private static void ClipTest(){
		}

		static void Main(string [] args){

			//abfragen/2011-10-12/1200/ ausgabe/alle.json 0.5 ausgabe/mittlere.json false
			Console.WriteLine (args [2]);
			try{
				Convert.ToDouble (args [2]);}
			catch{
				if (args[2].Contains(","))
					Console.WriteLine ("komma error: try changing "+args[2]+" to "+args[2].Replace(',','.'));
				else
					Console.WriteLine ("komma error: try changing "+args[2]+" to "+args[2].Replace('.',','));
				return;
			}
			Console.WriteLine("usage: mono isochronefromfolder.exe pathtoisochromes/ Alloutputfile.json probabilty probout.json isformat");
			Console.WriteLine("usage: mono isochronefromfolder.exe input/ Alloutputfile.json 0.5 probout.json true");
			DateTime start = DateTime.Now;
			Isochrones isc = new Isochrones(args[0],Convert.ToBoolean(args[4]));
			DateTime endLoad = DateTime.Now;
			Console.WriteLine ("beforeInterserction");
			isc.intersect();
			Console.WriteLine ("intersercted");
			isc.sortByFreq();
			Console.WriteLine ("Saving...");
			DateTime endTime = DateTime.Now;
			isc.ToFile(args[1]);
			isc.getIsochroneWithProbability (Convert.ToDouble(args[2])).ToFile(args[3]);
			DateTime saveTime = DateTime.Now;
			Console.WriteLine ("executed. Output: " + args [1] + "\n" + args [3]);
			Console.WriteLine("loadtime= "+endLoad.Subtract(start));
			Console.WriteLine("calc= "+endTime.Subtract(endLoad));
			Console.WriteLine("save= "+saveTime.Subtract(endTime));

		}
	} 
}

