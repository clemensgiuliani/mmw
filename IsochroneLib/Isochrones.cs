﻿using System;
using System.IO;
using Newtonsoft.Json;
using ClipperLib;
using GeoJSON;
using GeoJSON.Net.Geometry;
using Newtonsoft.Json.Serialization;
using System.Linq;
using System.Collections.Generic;
using GeoJSON.Net.Feature;


namespace IsochroneLib
{
	using Path = List<IntPoint>;
	using Paths = List<List<IntPoint>>;
	using ListOfPolygons = List<List<List<IntPoint>>>;

	public class Isochrones{
		public List<Isochrone> list;
		private bool isIntersected=false;

		public Isochrones(){
			list = new List<Isochrone>();
		}


		public Isochrones(List<Isochrone> i){
			list = i;
		}
		public Isochrones(string [] fileList,bool fileNameHasDateAndDmax=false){
			list = new List<Isochrone> ();
			for (int j = 0; j < fileList.Length; j++) {
				if (fileNameHasDateAndDmax){
					Console.WriteLine ("string"+fileList [j]);
					string dmax = fileList [j].Substring(fileList [j].IndexOf ("§") + 1,fileList [j].LastIndexOf (".") - fileList [j].IndexOf ("§")-1);
					Console.WriteLine ("dmax:" + dmax);
					string datestr = fileList [j].Substring (fileList[j].LastIndexOf(@"/")+1, fileList [j].LastIndexOf (".")-fileList [j].LastIndexOf ("/"));
					datestr = datestr.Remove(datestr.IndexOf("§"));
					Console.WriteLine ("datestr:" + datestr);
					Console.WriteLine (DateTime.Parse (datestr));
					list.Add (new Isochrone (
						fileList [j],
						DateTime.Parse (datestr),
						Convert.ToInt32(dmax)
					));}
				else
					list.Add (new Isochrone (fileList [j]));
				Console.WriteLine ("loaded "+fileList [j]);
			}
		}

		public Isochrones(string folderName,bool fileNameHasDateAndDmax):this(Directory.GetFiles(folderName),fileNameHasDateAndDmax){
		}


		public void intersect(){
			this.list = getIntersections (this.list);
			this.isIntersected = true;
		}
		public void sortByFreq(){
			list = list.OrderBy(i1=>i1.frequency).ToList();
		}
		public void ToFile(string filename){

			saveListOfIsochronesToFile (list, filename);
		}

		public void ToFile(string filename,DateTime first,DateTime last){

			saveListOfIsochronesToFile (list, filename,first,last);
		}
		public Isochrone getIsochroneWithFrequency(int f){
			//if (list.ElementAt(f).frequency==f)g
			//	return list.ElementAt(f);
			if (!isIntersected)
			this.sortByFreq ();
			return list.ElementAt(f-1);
		}
		public Isochrone getIsochroneWithProbability (double probability){
			int freq= Convert.ToInt32(Math.Round((this.Count()*probability)-0.5));
			if (!isIntersected)
				this.sortByFreq ();
			return this.getIsochroneWithFrequency(freq);

		}
		public int Count(){
			return list.Count;
		}



		private static List<Isochrone> getIntersections(List<Isochrone> notInetrsected){
			List<Isochrone> intersected = new List<Isochrone> ();
			intersected.Add (notInetrsected.First());
			DateTime startTime = DateTime.Now;
			for (int i = 1; i < notInetrsected.Count; i++) { 
				//Console.WriteLine ("i = " + i);
				Console.WriteLine ((i+1)+","+(DateTime.Now-startTime).TotalMilliseconds);
				Isochrone i0 = notInetrsected.ElementAt(i);
				for (int j = intersected.Count - 1; j >= 0; j--) {
					Isochrone i1 = new Isochrone ();
					Clipper c = new Clipper ();
					c.AddPaths (i0.isochronePaths, PolyType.ptSubject, true);
					c.AddPaths (intersected.ElementAt(j).isochronePaths, PolyType.ptClip, true);
					c.Execute (ClipType.ctIntersection ,i1.isochronePaths);
					i1.frequency = intersected.ElementAt (j).frequency + 1;
					if (j + 1 < intersected.Count ()) {
						c.Clear ();
						c.AddPaths (intersected.ElementAt (j + 1).isochronePaths, PolyType.ptSubject, true);
						c.AddPaths (i1.isochronePaths, PolyType.ptClip, true);
						c.Execute (ClipType.ctUnion, intersected.ElementAt (j + 1).isochronePaths);
					} else
						intersected.Add (i1);
					/*bool added = false;
					for (int k = 0; k < intersected.Count(); k++) {																																																																																																																																																																																																																																																																																																																																																													
						if (i1.frequency == intersected.ElementAt(k).frequency) {
							c.Clear ();
							c.AddPaths (intersected.ElementAt(k).isochronePaths, PolyType.ptSubject, true);
							c.AddPaths (i1.isochronePaths, PolyType.ptClip, true);
							c.Execute (ClipType.ctUnion, intersected.ElementAt(k).isochronePaths);
							k = intersected.Count;
							added = true;																																														
						}
					}
					if (!added) {
						intersected.Add (i1);
					}*/
				}
				/*if (i0.isochronePaths.Count () > 0) {
					for (int k = 0; k < intersected.Count; k++) {
						if (intersected.ElementAt(k).frequency == i0.frequency) {
							Clipper c = new Clipper ();
							c.AddPaths (i0.isochronePaths, PolyType.ptSubject, true);
							c.AddPaths (intersected.ElementAt(k).isochronePaths, PolyType.ptClip, true);
							c.Execute (ClipType.ctUnion, intersected.ElementAt(k).isochronePaths);
							k = intersected.Count + 1;
						}
					}

				}*/
				Clipper cl = new Clipper();
				cl.AddPaths (i0.isochronePaths, PolyType.ptSubject, true);
				cl.AddPaths (intersected.ElementAt(0).isochronePaths, PolyType.ptClip, true);
				cl.Execute (ClipType.ctUnion, intersected.ElementAt(0).isochronePaths);
			}
			return intersected;

		}

		private static void saveListOfIsochronesToFile (List<Isochrone> iList,string fileName,DateTime first,DateTime last){
			List < Feature > listF = new List<Feature> ();
			foreach (Isochrone i in iList) {
				listF.Add (i.ToFeature (first,last));

			}


			FeatureCollection fc = new FeatureCollection (listF);

			string serializedData = JsonConvert.SerializeObject(fc, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver(), NullValueHandling = NullValueHandling.Ignore });

			//Console.WriteLine (serializedData);
			StreamWriter file = File.CreateText(fileName);
			file.Write (serializedData);
			file.Close();
		}

		private static void saveListOfIsochronesToFile (List<Isochrone> iList,string fileName){
			List < Feature > listF = new List<Feature> ();
			foreach (Isochrone i in iList) {
				listF.Add (i.ToFeature ());

			}


			FeatureCollection fc = new FeatureCollection (listF);

			string serializedData = JsonConvert.SerializeObject(fc, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver(), NullValueHandling = NullValueHandling.Ignore });

			//Console.WriteLine (serializedData);
			StreamWriter file = File.CreateText(fileName);
			file.Write (serializedData);
			file.Close();
		}
		public void cleanup (){
			for (int i = this.list.Count - 1; i >= 0; i--) {
				this.list.ElementAt (i).cleanup();
				if (this.list.ElementAt (i).isochronePaths.Count == 0){
					this.list.RemoveAt (i);
				}
			}
		}


	}
}
