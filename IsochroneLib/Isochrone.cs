﻿
using System;
using System.IO;
using Newtonsoft.Json;
using ClipperLib;
using GeoJSON;
using GeoJSON.Net.Geometry;
using Newtonsoft.Json.Serialization;
using System.Linq;
using System.Collections.Generic;
using GeoJSON.Net.Feature;


namespace IsochroneLib
{

	using Path = List<IntPoint>;
	using Paths = List<List<IntPoint>>;
	using ListOfPolygons = List<List<List<IntPoint>>>;

	public class Isochrone{
		const int NK=1000000; //NUMBER OF KOMMASTELLEN
		public int id=0;
		public Paths isochronePaths;
		public int frequency = 1;
		public int dmax = 15;//15 minutes
		public DateTime queryTime=new DateTime(0);
		public List<DateTime> times;

		public Isochrone(){
			this.isochronePaths=new Paths ();
			this.times = new List<DateTime> ();
		}

		public Isochrone(string fileName){
			ListOfPolygons pl = getListFromFile (fileName);
			this.isochronePaths=getPaths(pl);
		}

		public Isochrone(string fileName, DateTime dt){
			ListOfPolygons pl = getListFromFile (fileName);
			this.isochronePaths=getPaths(pl);
			this.queryTime = dt;
			this.times = new List<DateTime> ();
			times.Add (queryTime);
		}
		public Isochrone(string fileName, DateTime dt, int dmax){
			ListOfPolygons pl = getListFromFile (fileName);
			this.isochronePaths=getPaths(pl);
			this.queryTime = dt;
			this.dmax = dmax;
			this.times = new List<DateTime> ();
			times.Add (queryTime);
		}

		public long numberOfPoints(){
			long numberofpoints = 0;
			foreach (Path p in isochronePaths) {
				numberofpoints = numberofpoints + p.Count();
			}
			return numberofpoints;
		}

		public long numberOfPolygons(){
			return isochronePaths.Count ();
		}

		public void cleanup(){
			for (int i = this.isochronePaths.Count - 1; i >= 0; i--) {
				//Console.WriteLine (i);
				//Console.WriteLine ("count"+this.isochronePaths.ElementAt (i).Count);
				if (this.isochronePaths.ElementAt (i).Count < 10) {
					//Console.WriteLine ("removed");
					this.isochronePaths.RemoveAt(i);
				}
			}
			/*
			for (int k = isochronePaths.Count-1;k>=0;k--) {
				Path p = this.isochronePaths.ElementAt(k);
					int i,j;
					double area = 0; 

					for (i=0; i < p.Count; i++) {
					j = (i + 1) % p.Count;
					GeographicPosition myGPi= IntPointToIPosition(p.ElementAt(i));
					GeographicPosition myGPj= IntPointToIPosition(p.ElementAt(j));
					area += myGPi.Latitude * myGPj.Latitude;
					area -= myGPi.Longitude * myGPj.Longitude;
					}

					area /= 2;
				Console.WriteLine("Area:"+ (area < 0 ? -area : area));
				if (area < 1) {
					//this.isochronePaths.RemoveAt (k);
				}
			}*/


		}


		public void ToFile(string filename){
			MultiPolygon outputMP = ListOfPathsToMultiPolygon (new ListOfPolygons(){isochronePaths});
			Console.WriteLine (outputMP);
			saveMultiPolygonToFile (outputMP,filename);
		}

		public Feature ToFeature(){
			MultiPolygon outputMP = ListOfPathsToMultiPolygon (new ListOfPolygons(){isochronePaths});
			var newFeature = new Feature(outputMP);
			newFeature.Properties.Add ("frequency", this.frequency);
			return newFeature;
		}

		public Feature ToFeature(DateTime first,DateTime last){
			MultiPolygon outputMP = ListOfPathsToMultiPolygon (new ListOfPolygons(){isochronePaths});
			var newFeature = new Feature(outputMP);
			double[] stat = this.getSkewness (first, last);
			newFeature.Properties.Add ("frequency", this.times.Count);
			newFeature.Properties.Add ("id", this.id);
			newFeature.Properties.Add ("mean", stat[0]);
			newFeature.Properties.Add ("median", stat[1]);
			newFeature.Properties.Add ("stddev", stat[2]);
			newFeature.Properties.Add ("skew", stat[3]);

			return newFeature;
		}

		private double [] getSkewness(DateTime first, DateTime last){
			return getSkeewness (this.times.ToArray (), first, last);
		}

		public static double []getSkeewness(DateTime [] list, DateTime start, DateTime end){
			//calculation of intervals between single "connections", skewness coefficient is then calculated on 
			//that list; reason is to include interval between start and first connection; same with end.
			List<double> deltaList = new List<double> ();
			if (!list [0].Equals (start))
				deltaList.Add ((list [0].Subtract (start)).TotalSeconds);
				
			for (int i = 0; i < list.Length-1; i++) {
				deltaList.Add(list[i+1].Subtract(list [i]).TotalSeconds);
				//delta [i] = list [i + 1] - list [i];
			}
			if (!list [list.Length-1].Equals (end))
				deltaList.Add ((end.Subtract (list [list.Length-1])).TotalSeconds);

			//double[] delta = deltaList.ToArray ();

			double mean = Mean (deltaList);
			double median = Median (deltaList);
			double stddev = StandardDeviation(deltaList);

			double[] returnArray = new double[4];
		
			returnArray [0] =mean;
			returnArray [1] =median;
			returnArray [2] = stddev; 
			if ((mean - median) == 0)
				returnArray [3] =  0.0;
			else
				returnArray[3]=(3 * (mean - median) / stddev);

			return returnArray;

		}
		public static double Mean(List<double> source) 
		{ 
			return source.Sum()/source.Count;
		}
		public static double StandardDeviation(List<double> source) 
		{ 
			return Math.Sqrt(Variance(source));
		}
		public static double Variance(List<double> source) 
		{ 
			int n = 0;
			double mean = 0;
			double M2 = 0;

			foreach (double x in source)
			{
				n = n + 1;
				double delta = x - mean;
				mean = mean + delta / n;
				M2 += delta * (x - mean);
			}
			return M2 / (n - 1);
		}
		public static double Median(List<double> source) 
		{ 
			var sortedList = from number in source 
				orderby number 
				select number; 

			int count = sortedList.Count(); 
			int itemIndex = count / 2; 
			if (count % 2 == 0) // Even number of items. 
				return (sortedList.ElementAt(itemIndex) + 
					sortedList.ElementAt(itemIndex - 1)) / 2; 

			// Odd number of items. 
			return sortedList.ElementAt(itemIndex); 
		}
		private static GeographicPosition IntPointToIPosition(IntPoint p){
			double longitude = (double)p.X / NK;
			double latitude = (double)p.Y / NK;

			return new GeographicPosition (latitude,longitude);

		}
		private static MultiPolygon ListOfPathsToMultiPolygon(ListOfPolygons lp){
			List<Polygon> polygonList = new List<Polygon> ();
			foreach (Paths pts in lp) {
				List<LineString> lineStringList = new List<LineString> ();
				foreach (Path pt in pts){
					List<GeographicPosition> geoList = new List<GeographicPosition>();
					foreach (IntPoint ip in pt) {
						geoList.Add(IntPointToIPosition(ip));
					}
					geoList.Add (geoList.First());
					lineStringList.Add(new LineString(geoList.ToList<IPosition>()));

				}
				polygonList.Add(new Polygon(lineStringList));
			}
			return new MultiPolygon(polygonList);
		} 

		private static Int64[] IposToInt64(IPosition ps){
			Int64[]pos = new Int64[2];
			string longitude = (ps.ToString ().Substring (ps.ToString ().IndexOf (":") + 2, ps.ToString ().IndexOf (",") - 11));
			/*
			while (longitude.Length - longitude.IndexOf (".") < 14) {
				longitude = longitude + "0";
			}*/
			longitude = longitude.Substring (0, longitude.IndexOf (".") + NK+1);


			pos [0] = Convert.ToInt64 (longitude.Replace (".", ""));
			//pos[0] = Convert.ToInt64((ps.ToString().Substring(ps.ToString().IndexOf(":")+2,ps.ToString().IndexOf(",")-11)).Replace(".",""));
			//Console.WriteLine (pos[0]);
			string latitude = (ps.ToString().Substring(ps.ToString().LastIndexOf(":")+2, ps.ToString().Length-ps.ToString().LastIndexOf(":")-2));
			/*
			while (latitude.Length - latitude.IndexOf (".") < 14) {
				latitude = latitude + "0";
			}*/
			latitude = latitude.Substring (0, latitude.IndexOf (".") + NK+1);

			pos[1] = Convert.ToInt64(latitude.Replace(".",""));
			//pos [0] /= 1000000000;
			//pos [1] /= 1000000000;
			//Console.WriteLine (pos[0]);
			return pos;
		}
		static IntPoint geographicPositionToIntPoint(GeographicPosition gps){
			double longitude = (double)gps.Longitude*NK;
			double latitude = (double)gps.Latitude*NK;
			return new IntPoint ((int)longitude,(int)latitude);

		}


		private static string OpenJson(string fileName){
			StreamReader file = File.OpenText(fileName);
			string jsonString = file.ReadToEnd();
			file.Close();


			return jsonString.Substring(jsonString.IndexOf("{\"type\":\"MultiPolygon\""),jsonString.IndexOf("}")-jsonString.IndexOf("{\"type\":\"MultiPolygon\"")+1);

		}
		private static ListOfPolygons getListFromFile(string filename){
			string jsonString= OpenJson(filename);

			//Console.WriteLine (jsonString);
			MultiPolygon mpg = JsonConvert.DeserializeObject<MultiPolygon>(jsonString,new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

			ListOfPolygons polyList= new ListOfPolygons(mpg.Coordinates.Count());
			//Console.WriteLine ("number of polygons: " + mpg.Coordinates.Count ());

			foreach (Polygon pg in mpg.Coordinates) {

				Paths subj = new Paths(pg.Coordinates.Count());
				foreach (LineString ls in pg.Coordinates){
					Path pa= new Path(ls.Coordinates.Count());
					foreach (GeographicPosition ps in ls.Coordinates){
						//Console.WriteLine (IposToDouble(ps)[0]);
						//Console.WriteLine (IposToDouble(ps)[1]);

						IntPoint ip = geographicPositionToIntPoint (ps);//new IntPoint(IposToInt64(ps)[0],IposToInt64 (ps) [1]);
						if (pa.Count==0||(Math.Sqrt(Math.Pow((ip.X-pa.Last().X),2)+Math.Pow((ip.Y-pa.Last().Y),2))) > 0){
							pa.Add(ip);}

					}
					pa.Remove (pa.Last());
					subj.Add (pa);
				}
				polyList.Add (subj);
			}
			return polyList;
		}

		static void saveMultiPolygonToFile(MultiPolygon mp,string filename){
			var newFeature = new Feature(mp);
			//newFeature.Properties.Add ("importance", (int)50);
			string serializedData = JsonConvert.SerializeObject(newFeature, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver(), NullValueHandling = NullValueHandling.Ignore });

			//Console.WriteLine (serializedData);
			StreamWriter file = File.CreateText(filename);
			file.Write (serializedData);
			file.Close();


		}
		private static Paths getPaths(ListOfPolygons lp){

			Clipper c = new Clipper ();
			foreach (Paths p in lp) {

				c.AddPaths (p, PolyType.ptSubject, true);

			}
			Paths solution = new Paths ();
			c.Execute (ClipType.ctUnion, solution);
			return solution;

		}
	}
}